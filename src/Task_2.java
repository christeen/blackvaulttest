public abstract class Task_2 {

    static int foo(int n) {

        if(n <= 1) {
            return n;
        } else {
            return foo(n - 1) + foo(n - 2);
        }
    }

    public static void main(String[] args) {
        int N = 10;
        for (int i = 0; i < N; i++) {
            System.out.println(foo(i));
        }
    }

}
